package com.example.mindxcontest.model;

import lombok.Data;

@Data
public class Account {
  private String username;
  private String password;
}
