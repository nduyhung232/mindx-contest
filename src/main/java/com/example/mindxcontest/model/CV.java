package com.example.mindxcontest.model;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class CV {
  private UUID id;
  private Infomation infomation;
  private String summary;
  private Experiences experiences;
  private Education education;
  private String skills;
}
