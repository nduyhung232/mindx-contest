package com.example.mindxcontest.model;

import lombok.Data;

@Data
public class Token {
  private String token;
  private static final Token INSTANCE = new Token();

  private Token() {

  }

  public static Token getInstance() {
    return INSTANCE;
  }
}
