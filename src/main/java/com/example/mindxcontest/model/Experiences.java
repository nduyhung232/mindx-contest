package com.example.mindxcontest.model;

import lombok.Data;

@Data
public class Experiences {
  private String jobTitle;
  private String company;
  private String period;
  private String jobDescription;
}
