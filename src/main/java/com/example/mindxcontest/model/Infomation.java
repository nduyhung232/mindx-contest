package com.example.mindxcontest.model;

import lombok.Data;

@Data
public class Infomation {
  private String name;
  private String email;
  private String mobile;
  private String github;
  private String linkedIn;
}
