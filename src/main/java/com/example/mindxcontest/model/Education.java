package com.example.mindxcontest.model;

import lombok.Data;

@Data
public class Education {
  private String universityOrCollege;
  private String faculty;
  private String gpa;
}
