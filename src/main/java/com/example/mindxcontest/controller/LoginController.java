package com.example.mindxcontest.controller;

import com.example.mindxcontest.exception.NotFoundException;
import com.example.mindxcontest.model.Account;
import com.example.mindxcontest.model.Token;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/login")
public class LoginController {

  @PostMapping()
  public String login(@RequestBody Account account) {
    if (account.getUsername().equals("user") && account.getPassword().equals("pass")) {
      UUID uuid = UUID.randomUUID();
      Token.getInstance().setToken(uuid.toString());
      return uuid.toString();
    } else {
      throw new NotFoundException("Tài khoản hoặc mật khẩu không đúng !");
    }
  }
}
