package com.example.mindxcontest.controller;

import com.example.mindxcontest.exception.NotFoundException;
import com.example.mindxcontest.exception.SystemErrorException;
import com.example.mindxcontest.exception.UnauthorizedException;
import com.example.mindxcontest.model.CV;
import com.example.mindxcontest.model.Token;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/cv")
public class Controller {
  static List<CV> data = new ArrayList<>();

  @GetMapping()
  public List<CV> getListCv(@RequestHeader("token") String token) {
    // check authentication
    checkAuthentication(token);

    return data;
  }

  @GetMapping("/{id}")
  public CV getCv(@RequestHeader("token") String token,
                  @PathVariable("id") String id) {
    // check authentication
    checkAuthentication(token);

    // progress
    CV cv = null;
    if (data.size() > 0)
      for (int i = 0; i < data.size(); i++) {
        CV ele = data.get(i);
        if (ele.getId().toString().equals(id)) {
          cv = ele;
        }
      }
    if (cv != null) {
      return cv;
    }
    throw new NotFoundException("Không tìm thấy CV có id là " + id);

  }

  @PostMapping()
  public ResponseEntity<CV> saveCv(@RequestHeader("token") String token,
                                   @RequestBody CV cv) {
    // check authentication
    checkAuthentication(token);

    // progress
    UUID uuid = UUID.randomUUID();
    cv.setId(uuid);
    if (data.add(cv)) {
      return ResponseEntity.ok(cv);
    }
    throw new SystemErrorException("Hệ thống không thể thực hiện được tác vụ");
  }

  @PutMapping()
  public ResponseEntity<CV> uploadCv(@RequestHeader("token") String token,
                                     @RequestBody CV cv) {
    // check authentication
    checkAuthentication(token);

    // progress
    UUID id = cv.getId();

    for (int i = 0; i < data.size(); i++) {
      CV ele = data.get(i);
      if (ele.getId().toString().equals(id.toString())) {
        data.set(i, cv);
        return ResponseEntity.ok(cv);
      }
    }
    throw new NotFoundException("Không tìm thấy CV có id là " + id);
  }

  @DeleteMapping("/{id}")
  public ResponseEntity deleteCv(@RequestHeader("token") String token,
                     @PathVariable("id") UUID id) {
    // check authentication
    checkAuthentication(token);

    // progress
    if (data.size() > 0)
      for (int i = 0; i < data.size(); i++) {
        CV ele = data.get(i);
        if (ele.getId().toString().equals(id.toString())) {
          data.remove(i);
          return ResponseEntity.ok("Đã xóa thành công CV có id là " + id);
        }
      }
    throw new NotFoundException("Không tìm thấy CV có id là " + id);
  }

  private boolean checkAuthentication(String token) {
    Token currentToken = Token.getInstance();
    if (currentToken.getToken() != null)
      if (currentToken.getToken().equals(token)) {
        return true;
      }
    throw new UnauthorizedException("Bạn không có quyền truy cập !");
  }
}
