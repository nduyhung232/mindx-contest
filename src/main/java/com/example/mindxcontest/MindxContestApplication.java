package com.example.mindxcontest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MindxContestApplication {

  public static void main(String[] args) {
    SpringApplication.run(MindxContestApplication.class, args);
  }

}
